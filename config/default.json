{
  "locale": "en",
  "commands": {
    "clue": {
      "description": "Give out a clue!",
      "usage": "-clue [word] [guesses]\n-clue banana 3"
    },
    "create": {
      "description": "Create a game on this channel!",
      "aliases": ["criar", "configure", "conf"],
      "usage": "-create [...Words?]\n-configure\n-configure Word1 Word2 Word3..."
    },
    "endTurn": {
      "description": "End your turn!"
    },
    "game": {
      "description": "Check info about the current game!"
    },
    "guess": {
      "description": "Guess a word!",
      "usage": "-guess plane"
    },
    "help": {
      "description": "Get help!"
    },
    "join": {
      "description": "Join the game!"
    },
    "leave": {
      "description": "Leave the game!"
    },
    "shuffle": {
      "description": "Randomize the teams and spymasters!",
      "aliases": ["random"]
    },
    "spymaster": {
      "description": "Become your team's spymaster!"
    },
    "start": {
      "description": "Start the game!"
    },
    "stop": {
      "description": "Stop the game!",
      "aliases": ["cancel", "end"]
    },
    "tutorial": {
      "description": "Learn how to play!"
    },
    "about": {
      "description": "About the bot",
      "aliases": ["version"]
    }
  },
  "messages": {
    "GAME_ALREADY_CONFIGURED": "A game has already been configured on this channel! Do `-game` to check it out!",
    "NO_ACTIVITY_WARNING": "🔌 | **Note: The lobby will automatically be disbanded if there is no activity.**",
    "GAME_DISBANDED": "** 📤 | Game disbanded. **",
    "GAME_STOPPED": "**:no_entry: | Game stopped!**",
    "MUTE_NOTICE": "please wait a few seconds before using another command!",
    "GAME_REQUIRED": "This channel must have a game configured in order to use this command!",
    "ERROR_OCCURRED": "An error occurred!",
    "HELP": "```ml\n'Command' \"List\"```\n\nUse `-help [command]` to get more information on a command.\n\n**1. Information -** `help`, `game`, `tutorial`\n**2. Lobby -**  `configure`, `join`, `leave`, `spymaster`, `start`, `stop`, `shuffle`\n**3. Game -** `clue`,`guess`, `endturn`\n`",
    "INVALID_COMMAND": "This command doesn't exist!",
    "PERMISSION_GAME_REQUIRED": "- A game needs to be configured on this channel\n",
    "PERMISSION_TURN_REQUIRED": "- It must be your team's turn\n",
    "USAGE": "Usage",
    "ALIASES": "Aliases",
    "RESTRICTIONS": "Restrictions",
    "RED": "Red team \uD83D\uDD34",
    "BLUE": "Blue team \uD83D\uDD35",
    "CLUE": "\uD83D\uDD0D Clue",
    "GUESS": "Guess",
    "PASS": "➡\uFE0F Pass",
    "BOARD": "Board",
    "JOIN_RED": "Join/Leave Red",
    "JOIN_BLUE": "Join/Leave Blue",
    "RED_TURN": "Red team's turn \uD83D\uDD34",
    "BLUE_TURN": "Blue team's turn \uD83D\uDD35",
    "SHUFFLE": "Shuffle",
    "START": "Start",
    "SPY": "\uD83D\uDC41 See who's who",
    "CLUE_BTN": "\uD83D\uDC41 See who's who",
    "WORD": "Word",
    "NUMBER": "Number",
    "RED_WINS": "\uD83D\uDD34 Red team wins! \uD83D\uDD34",
    "BLUE_WINS": "\uD83D\uDD35 Blue team wins! \uD83D\uDD35",

    "TUTORIAL_1": "```ml\n'Codenames' \"Tutorial\"```\n\nWelcome to the codenames bot tutorial! Before we begin, this game is **exactly the same** as the board game. If you don't know how to play the board game, read this: https://en.wikipedia.org/wiki/Codenames_(board_game).\n\n__Send Anything To Continue__",
    "TUTORIAL_2": "```ml\n'Codenames' \"Tutorial\"```\n\nTo create a game lobby, do `-configure` in the channel you want to play in. Only **one** game can be played in the same channel at a time. You can start the game (`-start`), 'shuffle' the players by placing them on random teams (`-shuffle`) and end/stop/destroy the game lobby (`-stop`). You can add custom words by providing them as arguments: `-configure [...words]`. Example: `-configure word1 word2 word3`\n\n__Send Anything To Continue__",
    "TUTORIAL_3": "```ml\n'Codenames' \"Tutorial\"```\n\nOnce the lobby has been created, players can do `-join [team]`, where `[team]` is either `red` or `blue`, to join. If a player wants to become the spymaster for their team, they do `-spymaster`.\n\n__Send Anything To Continue__",
    "TUTORIAL_4": "```ml\n'Codenames' \"Tutorial\"```\n\nThis is what the spymasters see at the start of every turn: https://imgur.com/vCfvr1g\n\nThe words in red are `red agents`, the words in blue are `blue agents`, the words in white are `bystanders`, and the word with __black background__ is the `assassin`. Before the spymaster's team can guess, the spymaster must give out a clue and the amount of guesses with `-clue [clue] [guesses]`, where `[clue]` is a one word clue and `[guesses]` is the number of guesses their team has this round. Example: `-clue apple 2`\n This is what the spymasters see later on, where guesses have been made: https://imgur.com/7F3cTb6\n\n__Send Anything To Continue__",
    "TUTORIAL_5": "```ml\n'Codenames' \"Tutorial\"```\n\nOnce the spymaster has given out the clue, the players of their team have to make guesses. They do that with the `-guess [word]` command. Example: `-guess microwave`. Players can also end their turn if they want to with the `-endturn` command. Players must make at least one guess before ending their turn.\n\n__Send Anything To Continue__",
    "TUTORIAL_6": "```ml\n'Codenames' \"Tutorial\"```\n\nAnd that's it! Have fun using the bot!\n\nTo see the list of all commands, do: `-help`",

    "REPEATING_WORDS": "There can't be any repeats!",
    "MAXIMUM_WORDS_EXCEDED": "The maximum amount of custom words is `25`!",
    "WORD_TOO_LONG": "One of the custom words is too long! Maximum length is `16`!",

    "INVALID_TEAM": "Available teams: $teamNames",
    "ALREADY_ON_TEAM": "You are already on this team!",
    "JOINED_TEAM": "$userName successfully joined the $teamName team!",

    "NOT_IN_GAME": "You aren't in the game!",
    "SPYMASTER_CANT_LEAVE": "The spymaster cannot leave once the game has started!",
    "LEFT_GAME": "Successfully removed you from the game!",

    "GAME_ALREADY_STARTED": "The game has already started!",
    "TEAM_REQUIRED": "You must be in a team!",
    "BECAME_SPYMASTER": "You are now the spymaster for the `$teamName` team!",

    "FIELD_AGENTS_REQUIRED": "All teams must have at least one field agent assigned!",
    "SPYMASTERS_REQUIRED": "All teams must have a spymaster assigned!",
    "NEXT_TURN": "`$teamName` ($spymasterName), it's your turn!",

    "GAME_NOT_STARTED": "The game hasn't started!",
    "NOT_SPYMASTER": "You must be the team's spymaster in order to do that!",
    "NOT_TEAMS_FIELD_AGENT": "You must be a team's field agent in order to do that!",
    "NOT_YOUR_TURN": "You can do this when it's your turn!",
    "CLUE_ALREADY_PROVIDED": "You already gave a clue to your team!",
    "CLUE_NUMBER_REQUIRED": "You need to provide the amount of guesses your team will have!",
    "CLUE_IN_BOARD": "You can't use a word on the board as a clue!",
    "CLUE_MESSAGE": "Clue for the **$teamName**: **$clueWord** ($clueNumber)",

    "WORD_REQUIRED": "You need to provide a word!",
    "FIELD_AGENT_REQUIRED_TO_GUESS": "The spymaster cannot guess words!",
    "NO_CLUE": "The spymaster hasn't given the clue!",
    "WORD_NOT_IN_BOARD": "`$guessedWord` isn't on the board!",
    "WORD_ALREADY_GUESSED": "The word `$guessedWord` has already been guessed!",

    "ASSASSIN": "Noo! Codename `$guessedWord` was the `assassin`!",
    "BYSTANDER": "Oops! The word `$guessedWord` is a bystander!",
    "ENEMY_AGENT": "Oops! Codename `$guessedWord` is an enemy `$enemyTeamName` agent!",
    "AGENT": "Nice! Codename `$guessedWord` is a `$teamName` agent!",
    "GUESSES_LEFT": "Your team has `$guessesLeft` guesses left!",
    "WINNER": "`$teamName` ($playerNames) wins!",

    "SPYMASTER_CANT_END_TURN": "The spymaster cannot end the turn!",
    "GUESS_REQUIRED": "Your team must make at least one guess!",

    "ABOUT": "Version: $version\n\nThis bot was developed during the isolation caused by COVID-19 toto chase away loneliness and gather friends.\nFound a bug? Create an issue here: `https://gitlab.com/cyrogoncalves/discord-codenames/-/issues/new`\nEnjoying the bot? Upvote it: https://discordbots.org/bot/TODO"
  }
}