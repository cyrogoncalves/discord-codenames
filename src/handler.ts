import * as Discord from "discord.js";
import * as config from "config";
import * as fs from "fs";
// import {createClient} from "redis";

// const client = createClient({url: process.env.REDIS_URL})
//     .on("error", (error) => console.error(error));
// const persist = (message: Discord.Message, game: Game) =>

const msg = config.get<{[propName in string]: string}>("messages");
// const cmd = config.get<{[p in string]: CommandData}>("commands");
const wordsPath = `./assets/word-pool.${config.get<string>("locale")}.txt`;
const WORD_POOL = fs.readFileSync(wordsPath, {encoding: 'utf8'}).split(/\n|\r\n/);

const PRIMARY = 1, SECONDARY = 2, SUCCESS = 3, DANGER = 4;
const btn = (customId: string, label: string, style: number, enabled: boolean=true) =>
  new Discord.ButtonBuilder().setLabel(label).setDisabled(!enabled).setCustomId(customId).setStyle(style)
const row = <T extends Discord.AnyComponentBuilder>(...components: T[]) =>
  new Discord.ActionRowBuilder<T>().addComponents(...components)
const input = (customId: string, label: string, placeholder: string) =>
  new Discord.ActionRowBuilder<Discord.TextInputBuilder>().addComponents(
    new Discord.TextInputBuilder().setCustomId(customId).setLabel(label).setRequired(true)
      .setStyle(Discord.TextInputStyle.Short).setPlaceholder(placeholder))
const modal = (customId: string, title: string,
               ...components: Discord.ActionRowBuilder<Discord.TextInputBuilder>[]) =>
  new Discord.ModalBuilder().setCustomId(customId).setTitle(title).addComponents(components)
const option = (value: string, label: string = value) =>
  new Discord.StringSelectMenuOptionBuilder().setValue(value).setLabel(label)
const select = (customId: string, placeholder: string, options: Discord.StringSelectMenuOptionBuilder[]) =>
  new Discord.StringSelectMenuBuilder().setCustomId(customId).setPlaceholder(placeholder).addOptions(options)

const gameCard = (game: Game) => {
  if (!game.turns) {
    const embed = new Discord.EmbedBuilder().addFields(...game.teams.map((team, i) => ({
      name: i === 0 ? msg.RED : msg.BLUE,
      value: `${team[0]?.username ?? "-"} 🕵\n${team.slice(1).map(p => p.username).join("\n")}`,
      inline: true
    })))
    return {embeds: [embed], components: [row(
        btn("join1", msg.JOIN_RED, DANGER),
        btn("join2", msg.JOIN_BLUE, PRIMARY),
        btn("shuffle", msg.SHUFFLE, SECONDARY),
        btn("start", msg.START, SUCCESS,
          !!process.env["DEV_MODE"] || !game.teams.some(t => t.length === 0))
      )]};
  }

  const teamIdx = (game.turns.length - 1) % 2;
  const turn = game.turns[game.turns.length - 1];
  const embed = new Discord.EmbedBuilder()
    .setColor(!teamIdx ? 0xD13030 : 0x4183cc)

  embed.addFields(...game.teams.map((team, i) => ({
      name: `${!i ? msg.RED : msg.BLUE} ${game.cards.filter(c => c.team === i + 1 as 1 | 2 && !c.guessedBy).length}`,
      value: `${team[0]?.username ?? "-"} 🕵\n${team.slice(1).map(p => p.username).join("\n")}`,
      inline: true
    })))
  embed.addFields({name: "\u200B", value: "\u200B"})
  embed.addFields(...board(game.cards, !!game.winner))

  if (game.message) {
    embed.setFooter({ text: game.message })
    delete game.message
  }

  if (game.winner) {
    embed.setTitle(game.winner === 1 ? msg.RED_WINS : msg.BLUE_WINS)
    return {embeds: [embed], components: []}
  } else if (!turn.clue) {
    embed.setTitle(!teamIdx ? msg.RED_TURN : msg.BLUE_TURN)
    return {embeds: [embed], components: [row(
        btn("spy", msg.SPY, SECONDARY),
        btn("clue", msg.CLUE, !teamIdx ? DANGER : PRIMARY),
      )]};
  } else {
    embed.setTitle(msg.CLUE + `: ${turn.clue.word}:${turn.clue.number}`)
    const row1 = row(select("guess", "Guess a codename.",
      game.cards.filter(c => !c.guessedBy).map(c => option(c.word))));
    const row2 = row(btn("pass", msg.PASS, SECONDARY));
    return {embeds: [embed], components: [row1, row2]};
  }
}

const board = (cards: Card[], master=false) => {
  const chunks: Card[][] = []
  for (let i = 0; i < cards.length; i+=5)
    chunks.push(cards.slice(i, i + 5))
  return [...chunks.map(cards => ({
    name: "\u200B", value: cards.map(card=> {
      const emoji = !master && !card.guessedBy ? "❔"
        : ["🔴", "🔵", "👱", "👩", "👤"][[9, 17, 21, 24, 25].findIndex(n=>card.id<n)];
      return `${emoji} ${card.guessedBy ? `||${card.word}||` : `${card.word}`}`;
    }).join("\n"), inline: true
  })), { value:"\u200B", name:"\u200B", inline: true }]
}

const interactions = {
  join(teams: User[][], id: string, username: string, idx: number) {
    teams[0] = teams[0].filter(t => t.id !== id)
    teams[1] = teams[1].filter(t => t.id !== id)
    teams[idx].unshift({id, username})
  },
  shuffle(teams: User[][]) {
    const players = shuffle(teams.flat())
    teams[0] = players.slice(0, players.length / 2)
    teams[1] = players.slice(players.length / 2)
  },
  start(game: Game) {
    game.turns = [{guesses: []}]
    game.cards = shuffle(shuffle(WORD_POOL).slice(0, 25).map((word, id) => ({
      word, id, team: id < 9 ? 1 : id < 17 ? 2 : id < 24 ? 3 : 4
    })))
  },
  clue(game: Game, word: string, number: number) {
    game.turns[game.turns.length - 1].clue = {word, number};
  },
  guess(game: Game, guessedWord: string) {
    const teamId = (game.turns.length - 1) % 2 + 1 as 1 | 2;
    const turn = game.turns[game.turns.length - 1];
    const card = game.cards.find(w => w.word === guessedWord);
    card.guessedBy = teamId;
    turn.guesses.push(card);

    const msg0 = card.team === 4 ? msg.ASSASSIN : card.team === 3 ? msg.BYSTANDER
      : card.team === teamId ? msg.AGENT : msg.ENEMY_AGENT;
    game.message = format(msg0, { guessedWord })
    const winner = card.team === 4 /*assassin*/ ? teamId === 1 ? 1 : 0
      : game.teams.findIndex((_, i) => !game.cards.some(c => c.team === i+1 && !c.guessedBy))
    console.debug({card, winner})
    if (winner > -1) {
      game.winner = winner + 1 as 1 | 2
      game.message += " " + msg.WINNER
    } else if (card.team !== teamId || turn.clue.number > 0
      && turn.clue.number + 1 < turn.guesses.length) { // isTurnOver
      game.turns.push({guesses: []}) // endTurn
    } else {
      game.message += " " + msg.GUESSES_LEFT
    }
  }
}

const check = (i: Discord.ButtonInteraction, clause: boolean, content: string) => {
  const cant = !process.env["DEV_MODE"] && clause
  if (cant) i.reply({ content, ephemeral: true });
  return cant
}

module.exports = {
  initBot: () => {
    const bot = new Discord.Client({ intents: [
        Discord.GatewayIntentBits.Guilds,
        Discord.GatewayIntentBits.GuildMessages,
        Discord.GatewayIntentBits.MessageContent,
      ] });

    bot.on("interactionCreate", async i => {
      console.debug("interaction created: " + i)
      if (!i.isChatInputCommand()) return;
      try {
        if (i.commandName === "game") {
          const game: Game = { teams: [[], []] }

          const response = await i.reply(gameCard(game));
          const collector = response.createMessageComponentCollector(
            { componentType: Discord.ComponentType.Button, time: 3_600_000 });
          collector.on("collect", async i => {
            if (i.customId === "join1" || i.customId === "join2") {
              const username = i.channel.guild.members.cache.get(i.user.id).nickname || i.user.username;
              interactions.join(game.teams, i.user.id, username, i.customId === "join1" ? 0 : 1)
              await i.update(gameCard(game))
            } else if (i.customId === "shuffle") {
              interactions.shuffle(game.teams)
              await i.update(gameCard(game))
            } else if (i.customId === "start") {
              if (!process.env["DEV_MODE"] && game.teams.some(t => !t.length)) {
                i.reply({ content: msg.SPYMASTERS_REQUIRED, ephemeral: true });
                return
              }
              interactions.start(game)
              await i.update(gameCard(game))
            } else if (i.customId === "clue") {
              const team = game.teams[(game.turns.length - 1) % 2];
              if (!process.env["DEV_MODE"] && team[0].id !== i.user.id) {
                i.reply({ content: msg.NOT_SPYMASTER, ephemeral: true });
                return
              }

              await i.showModal(modal("clueModal", msg.CLUE,
                input("word", msg.WORD, "Ex.: hospital"),
                input("number", msg.NUMBER, "Ex.: 2")));

              const submitted = await i.awaitModalSubmit({time: 60_000})
                .catch(error => { console.error(error); return null })
              if (submitted) {
                const word = submitted.fields.getTextInputValue("word");
                const clueNumberStr = submitted.fields.getTextInputValue("number");
                // console.debug({word, clueNumberStr})
                if (isNaN(clueNumberStr)) {
                  submitted.reply({ content: msg.CLUE_NUMBER_REQUIRED, ephemeral: true });
                  return
                }
                interactions.clue(game, word, Number(clueNumberStr))
                await submitted.update(gameCard(game))
              }
            } else if (i.customId === "spy") {
              if (!game.teams.some(t=>t[0].id===i.user.id)) {
                i.reply({ content: msg.NOT_SPYMASTER, ephemeral: true });
                return
              }

              const embed = new Discord.EmbedBuilder().addFields(...board(game.cards, true));
              await i.reply({embeds: [embed], ephemeral: true})
            } else if (i.customId === "pass") {
              if (!game.turns[game.turns.length - 1].guesses.length) {
                i.reply({ content: msg.GUESS_REQUIRED, ephemeral: true });
                return
              }
              const team = game.teams[(game.turns.length - 1) % 2];
              if (!team.slice(1).some(u=>u.id === i.user.id)) {
                i.reply({ content: msg.NOT_TEAMS_FIELD_AGENT, ephemeral: true });
                return
              }

              game.turns.push({guesses: []}) // endTurn
              await i.update(gameCard(game))
            }
          });

          const guessCollector = response.createMessageComponentCollector(
            { componentType: Discord.ComponentType.StringSelect, time: 3_600_000 });
          guessCollector.on("collect", async i => {
            console.debug({guess: i.values})
            const teamId = (game.turns.length - 1) % 2 + 1 as 1 | 2;
            if (i.customId === "guess") {
              if (!process.env["DEV_MODE"] && !game.teams[teamId - 1].slice(1).some(u=>u.id === i.user.id)) {
                i.reply({ content: msg.NOT_TEAMS_FIELD_AGENT, ephemeral: true });
                return
              }
              interactions.guess(game, i.values[0])
            }
            await i.update(gameCard(game))
          });
        } else
          return console.error(`No command matching ${i.commandName} was found.`);
      } catch (error) {
        console.error(error);
        if (i.replied || i.deferred) {
          await i.followUp({ content: 'There was an error while executing this command!', ephemeral: true });
        } else {
          await i.reply({ content: 'There was an error while executing this command!', ephemeral: true });
        }
      }
    });

    bot.on("ready", async () => {
      // await client.connect();
      console.log(`READY!\nGuilds: ${bot.guilds.cache.size}\nUsers: ${bot.users.cache.size}`);
    });

    return bot.login(process.env["DISCORD_TOKEN"]);
  },
  refreshCommands: async () => {
    const gameCmd = new Discord.SlashCommandBuilder().setName("game")
      .setDescription("Starts a game or shows current game")
    const rest = new Discord.REST().setToken(process.env["DISCORD_TOKEN"]);
    console.log(`Started refreshing ${1} application (/) commands.`);
    const data = await rest.put(
      `/applications/${process.env["APP_ID"]}/commands`,
      { body: [gameCmd] },
    );
    console.debug({data})
    // console.log(`Successfully reloaded ${data.length} application (/) commands.`);
  }
};

export type Game = {
  teams: User[][],
  cards?: Card[],
  turns?: Turn[],
  winner?: 1 | 2;
  message?: string;
}
type User = {
  readonly id: string;
  readonly username: string;
}
type Turn = {
  readonly guesses: Card[],
  clue?: {
    word: string;
    number: number;
  }
}
export type Card = {
  readonly word: string
  readonly id: number; // 0 to 24
  readonly team: 1 | 2 | 3 | 4
  guessedBy?: 1 | 2
}

// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
export function shuffle<T>(array: T[]): T[] {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

export function format(msg: string, params: {[propName: string]: any}): string {
  return Object.entries(params).reduce((str, [k, v]) => str.replace(`$${k}`, v), msg);
}
