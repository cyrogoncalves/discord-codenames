import { RuleError, Game } from './codenames'

describe('Codenames', () => {

  const user = { id: 'userId', username: 'username' };
  const blueFieldAgent = { id: 'userId1', username: 'username1' };
  const blueSpymaster = { id: 'userId2', username: 'username2' };
  const redFieldAgent = { id: 'userId3', username: 'username3' };
  const redSpymaster = { id: 'userId4', username: 'username4' };

  const initGame = () => {
    const game = Game.create();
    const blueTeam = game.teams.find(t => t.name === 'blue');
    const redTeam = game.teams.find(t => t.name === 'red');
    return { game, blueTeam, redTeam };
  }

  const startGame = () => {
    const game = Game.create();
    game.join(blueSpymaster);
    game.join(redSpymaster);
    game.join(blueFieldAgent);
    game.join(redFieldAgent);
    game.start();
    return game;
  }

  describe('addFieldAgent', () => {
    it('throws error on invalid team name', () => {
      const { game } = initGame();
      expect(() => game.join(user, 'purple')).toThrow(RuleError);
      expect(() => game.join(user, 'purple')).toThrowError(/^Available teams: /);
    });

    it('adds player as field agent if not in game.', () => {
      const { game, blueTeam } = initGame();
      game.join(user, 'blue');
      expect(blueTeam.players.includes(user)).toBeTruthy();
    });

    it('when already field agent of another team, removes from current.', () => {
      const { game, blueTeam, redTeam } = initGame();
      game.join(user, 'blue');

      game.join(user, 'red');
      expect(blueTeam.players.includes(user)).toBeFalsy();
      expect(redTeam.players.includes(user)).toBeTruthy();
    });

    it('when already field agent for this team, throw "already in team" error.', () => {
      const { game } = initGame();
      game.join(user, 'red');

      expect(() => game.join(user, 'red')).toThrow(RuleError);
      expect(() => game.join(user, 'red')).toThrowError('You are already on this team!');
    });

    it('when the spy master of this team, throw "already in team" error.', () => {
      const { game } = initGame();
      game.join(user, 'red');
      game.spymaster(user);

      expect(() => game.join(user, 'red')).toThrow(RuleError);
      expect(() => game.join(user, 'red')).toThrowError('You are already on this team!');
    });

    it('when the spy master of another team, leave it.', () => {
      const { game, blueTeam, redTeam } = initGame();
      game.join(user, 'red');
      game.spymaster(user);

      game.join(user, 'blue');
      expect(redTeam.spymaster).toBeFalsy();
      expect(blueTeam.players.includes(user)).toBeTruthy();
    });
  });

  describe('addSpymaster', () => {
    it('when game has already started, throw error.', () => {
      const game = startGame();
      expect(() => game.spymaster(user)).toThrow(RuleError);
      expect(() => game.spymaster(user)).toThrowError('The game has already started!');
    });

    it('when not in a team, throw error.', () => {
      const { game } = initGame();
      expect(() => game.spymaster(user)).toThrow(RuleError);
      expect(() => game.spymaster(user)).toThrowError('You must be in a team!');
    });

    it('when a field agent, become spymaster and leave field agents.', () => {
      const { game, redTeam } = initGame();
      game.join(user, 'red');

      game.spymaster(user);
      expect(redTeam.spymaster).toBe(user);
      expect(redTeam.fieldAgents.includes(user)).toBeFalsy();
    });

    it('when there\'s already a spymaster, make them a field agent.', () => {
      const { game, redTeam } = initGame();
      game.join(blueSpymaster, 'red');
      game.spymaster(blueSpymaster);
      game.join(user, 'red');

      game.spymaster(user);
      expect(redTeam.spymaster).toBe(user);
      expect(redTeam.fieldAgents.includes(blueSpymaster)).toBeTruthy();
    });

    it('no error when already the spymaster.', () => {
      const { game, redTeam } = initGame();
      game.join(user, 'red');
      game.spymaster(user);

      game.spymaster(user);
      expect(redTeam.spymaster).toBe(user);
    });
  });

  describe('leave', () => {
    it('when not in a team, throw error.', () => {
      const { game } = initGame();
      expect(() => game.leave(user.id)).toThrow(RuleError);
      expect(() => game.leave(user.id)).toThrowError('You aren\'t in the game!');
    });

    it('when game has started and is spymaster, throw error.', () => {
      const game = startGame();

      expect(() => game.leave(blueSpymaster.id)).toThrow(RuleError);
      expect(() => game.leave(blueSpymaster.id)).toThrowError("The spymaster cannot leave once the game has started!");
    });

    it('when field agent, leave team.', () => {
      const { game, redTeam } = initGame();
      game.join(user, 'red');

      game.leave(user.id);
      expect(redTeam.fieldAgents.includes(user)).toBeFalsy();
    });

    it('when spymaster, leave team.', () => {
      const { game, redTeam } = initGame();
      game.join(user, 'red');
      game.spymaster(user);

      game.leave(user.id);
      expect(redTeam.spymaster).toBeFalsy();
    });
  });

  describe('start', () => {
    it('throws error on team with no field agents.', () => {
      const game = Game.create();
      game.join(blueFieldAgent, 'blue');
      game.join(redFieldAgent, 'red');

      expect(() => game.start()).toThrow(RuleError);
      expect(() => game.start()).toThrowError("All teams must have at least one field agent assigned!");
    });

    it('throws error on team with no spymaster.', () => {
      const game = Game.create();
      game.join(blueFieldAgent, 'blue');
      expect(() => game.start()).toThrow(RuleError);
      expect(() => game.start()).toThrowError("All teams must have a spymaster assigned!");
    });

    it('starts.', () => {
      const game = startGame();
      expect(game.currentTurn?.team).toBe(game.teams[0]);
    });

    it('throws error on game already started.', () => {
      const game = startGame();
      expect(() => game.start()).toThrow(RuleError);
      expect(() => game.start()).toThrowError("The game has already started!");
    });
  });

  describe('giveClue', () => {
    const game = startGame();

    it('throws error on game not started.', () => {
      const game = Game.create();
      expect(() => game.giveClue(user.id, ['word', '1'])).toThrow(RuleError);
      expect(() => game.giveClue(user.id, ['word', '1'])).toThrowError("The game hasn't ");
    });

    it('throws error if not a spymaster.', () => {
      expect(() => game.giveClue(redFieldAgent.id, ['word', '1'])).toThrow(RuleError);
    });

    it('throws error if it\'s not your turn.', () => {
      const user = game.teams[1].spymaster;
      expect(() => game.giveClue(user.id, ['word', '1'])).toThrow(RuleError);
    });

    it('throws error if the clue doesn\'t have a word and a number.', () => {
      expect(() => game.giveClue(game.teams[0].spymaster.id, [])).toThrow(RuleError);
    });

    it('throws error if the word provided is on the board.', () => {
      const wordOnBoard = game.cards[0].word;
      expect(() => game.giveClue(user.id, [wordOnBoard, '1'])).toThrow(RuleError);
    });

    it('allows already guessed word as clue.', () => {
      const card = game.cards[0];
      card.guessedBy = game.teams[0];
      game.giveClue(blueSpymaster.id, [card.word, '1'])
      expect(game.currentTurn.team).toBe(game.teams[0]);
      card.guessedBy = null;
    });

    it('registers clue.', () => {
      const game = startGame();
      game.giveClue(game.teams[0].spymaster.id, ['word', '1']);
      expect(game.currentTurn.clue.word).toBe('word');
      expect(game.currentTurn.clue.number).toBe(1);
    });

    it('throws error if a clue has already been provided this turn.', () => {
      const game = startGame();
      game.giveClue(game.teams[0].spymaster.id, ['word', '1']);
      expect(() => game.giveClue(game.teams[0].spymaster.id, ['word', '1'])).toThrow(RuleError);
    });
  });

  describe('guess', () => {
    const game = startGame();
    game.giveClue(game.currentTurn.team.spymaster.id, ['word', '2']);

    it('throws error on game not started.', () => {
      const game = Game.create();
      expect(() => game.guess(user.id, 'word')).toThrow(RuleError);
      expect(() => game.guess(user.id, 'word')).toThrowError("The game hasn't started!");
    });

    it('throws error on word not provided.', () => {
      const guess = () => game.guess(game.currentTurn.team.fieldAgents[0].id, undefined);
      expect(guess).toThrow(RuleError);
      expect(guess).toThrowError("You need to provide a word!");
    });

    it('throws error if spymaster.', () => {
      const spymasterGuess = () => game.guess(game.currentTurn.team.spymaster.id, 'word');
      expect(spymasterGuess).toThrow(RuleError);
      expect(spymasterGuess).toThrowError("The spymaster cannot guess words!");
    });

    it('throws error if not your turn.', () => {
      const guess = () => game.guess(game.teams[1].fieldAgents[0].id, 'word');
      expect(guess).toThrow(RuleError);
      expect(guess).toThrowError("You can do this when it's your turn!");
    });

    it('throws error if clue not provided.', () => {
      const game = startGame();

      const guess = () => game.guess(game.currentTurn.team.fieldAgents[0].id, 'word');
      expect(guess).toThrow(RuleError);
      expect(guess).toThrowError("The spymaster hasn't given the clue!");
    });

    it('throws error if word already guessed.', () => {
      const game = startGame();
      game.giveClue(game.currentTurn.team.spymaster.id, ['word', '2']);
      const rightGuess = game.cards.find(c => c.team?.name === game.currentTurn.team.name);
      const guesser = game.currentTurn.team.fieldAgents[0].id;
      game.guess(guesser, rightGuess.word);

      const guess = () => game.guess(guesser, rightGuess.word);
      expect(guess).toThrow(RuleError);
      expect(guess).toThrowError(`The word \`${rightGuess.word}\` has already been guessed!`);
    });

    it('registers right guess with guesses remaining.', () => {
      const game = startGame();
      const turnTeam = game.currentTurn.team;
      game.giveClue(turnTeam.spymaster.id, ['word', '2']);
      const rightGuess = game.cards.find(c => c.team?.name === turnTeam.name);
      const guesser = turnTeam.fieldAgents[0].id;

      game.guess(guesser, rightGuess.word);
      // expect(game.endTurn).toBeCalledTimes(0);
      expect(rightGuess.guessedBy.name).toBe(turnTeam.name);
      expect(game.currentTurn.team.name).toBe(turnTeam.name);
    });

    it('registers right guess with no guesses remaining.', () => {
      const game = startGame();
      const turnTeam = game.currentTurn.team;
      game.giveClue(turnTeam.spymaster.id, ['word', '1']);
      const [rightGuess1, rightGuess2] = game.cards.filter(c => c.team?.name === turnTeam.name);
      const guesser = turnTeam.fieldAgents[0].id;

      game.guess(guesser, rightGuess1.word);
      expect(rightGuess1.guessedBy.name).toBe(turnTeam.name);
      expect(game.currentTurn.clue).toBeTruthy();

      game.guess(guesser, rightGuess2.word);
      expect(rightGuess2.guessedBy.name).toBe(turnTeam.name);
      expect(game.currentTurn.clue).toBeFalsy();
    });

    it('ends game on assassin guess.', () => {
      const game = startGame();
      game.giveClue(game.currentTurn.team.spymaster.id, ['word', '1']);
      const guesser = game.currentTurn.team.fieldAgents[0].id;
      const guess = game.cards.find(c => c.type === 'assassin');
      game.guess(guesser, guess.word);
      expect(game.getWinner()).toBeTruthy();
    });

    it('ends turn on bystander guess.', () => {
      const game = startGame();
      game.giveClue(game.currentTurn.team.spymaster.id, ['word', '1']);
      const guesser = game.currentTurn.team.fieldAgents[0].id;
      const guess = game.cards.find(c => c.type === 'bystander');
      game.guess(guesser, guess.word);
      expect(game.isTurnOver()).toBeTruthy();
      expect(game.currentTurn.clue).toBeFalsy();
    });

    it('ends turn on enemy agent guess.', () => {
      const game = startGame();
      game.giveClue(game.currentTurn.team.spymaster.id, ['word', '1']);
      const guesser = game.currentTurn.team.fieldAgents[0].id;
      const guess = game.cards.find(c => c.type === 'agent' && c.team.name !== game.currentTurn.team.name);
      game.guess(guesser, guess.word);
      expect(game.isTurnOver()).toBeTruthy();
      expect(game.currentTurn.clue).toBeFalsy();
    });

    it('ends game on last right guess.', () => {
      const game = startGame();
      game.giveClue(game.currentTurn.team.spymaster.id, ['word', '10']);
      const guesser = game.currentTurn.team.fieldAgents[0].id;
      game.cards.filter(c => c.team?.name === game.currentTurn.team.name).map(c => game.guess(guesser, c.word));
      expect(game.getWinner()).toBeTruthy();
    });
  });

  describe('end turn', () => {
    const game = startGame();

    it('throws error on game not started.', () => {
      const game = Game.create();
      expect(() => game.endTurn(user.id)).toThrow(RuleError);
      expect(() => game.endTurn(user.id)).toThrowError("The game hasn't started!");
    });

    it('throws error if spymaster.', () => {
      const spymasterGuess = () => game.endTurn(game.currentTurn.team.spymaster.id);
      expect(spymasterGuess).toThrow(RuleError);
      expect(spymasterGuess).toThrowError("The spymaster cannot end the turn!");
    });

    it('throws error if not your turn.', () => {
      const guess = () => game.endTurn(game.teams[1].fieldAgents[0].id);
      expect(guess).toThrow(RuleError);
      expect(guess).toThrowError("You can do this when it's your turn!");
    });

    it('throws error if no guess was made this turn.', () => {
      const guess = () => game.endTurn(game.currentTurn.team.fieldAgents[0].id);
      expect(guess).toThrow(RuleError);
      expect(guess).toThrowError("You must make at least one guess!");
    });
  });

});